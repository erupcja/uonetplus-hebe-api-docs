import guzzle_sphinx_theme

project = "Unofficial docs for Vulcan UONET+ hebe API"
copyright = "2020, selfisekai and other contributors"
author = "selfisekai and other contributors"

html_theme_path = guzzle_sphinx_theme.html_theme_path()
html_theme = "guzzle_sphinx_theme"

# Register the theme as an extension to generate a sitemap.xml
extensions = [
    "guzzle_sphinx_theme",
]

# Guzzle theme options (see theme.conf for more information)
html_theme_options = {
    # Set the name of the project to appear in the sidebar
    "project_nav_name": "Docs for Ułomnet hebe API",
}

html_link_suffix = ""
